const console = require('./extras/console')

/**
  Ex 1. Escreva um algoritmo que leia um número digitado pelo 
  usuário e mostre a mensagem “Número maior do que 10! ”, caso 
  este número seja maior, ou “Número menor ou igual a 10! ”, 
  caso este número seja menor ou igual.
 */

// var numero = console.getNumber("Informe um número:")
// if (numero > 10){
//     console.log("Número maior do que 10")
// } else {1
//     console.log("Número menor ou igual a 10")
// }


// function mostrarMaior(a, b){
//     if (a == b) {
//         console.log("Sequência inválida!")
//     } else if (a > b) {
//         console.log(a);
//     } else {
//         console.log(b);
//     }
// }

var a = console.getNumber("A: ")
var b = console.getNumber("B: ")

//mostrarMaior(a, b);

if (a == b) {
    console.log("Sequência inválida!")
} else if (a > b) {
    console.log(a);
} else {
    console.log(b);
}