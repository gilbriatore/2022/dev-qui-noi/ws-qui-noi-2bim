/************************************************************************************
    AULA: Introdução ao Javascript

    1. Ambiente de desenvolvimento;
    2. Variáveis var, let, const, string, number e boolean;
    3. Operadores de atribuição, aritméticos, relacionais, lógicos e unários;
    4. Estruturas de decisão if/else, ternárias e switch;
    5. Estruturas de repetição while, do/while, for, for/in e for/of;
    6. Arrays e funções.
       Math.pow(3,2)
       Math.sqrt(64)
       Math.random()
       var value = Math.random() * 100;
       console.log(Math.round(value));
    7. Módulos.

    .code runner: ctrl + alt + n  para rodar o código
                  ctrl + alt + m  para interromper a execuação

 ***********************************************************************************/

// Variáveis var, let, const, string, number, boolean,

// let numero = 100;
// numero = 99;
// numero = "dez";
// numero = true;
// console.log(numero);

// operador de atribuição e aritméticos (=, +, -, *, /, %):


// var a = 10;
// var b = 5;

// console.log("a + b: " + ( a + b ));
// console.log("a - b: " + ( a - b ));
// console.log("a * b: " + ( a * b ));
// console.log("a / b: " + ( a / b ));
// //b = 4;
// console.log("a % b: " + ( a % b ));

// operadores de atribuição aritméticos (=, *=, /=, +=, -=):

// var a = 10;
// var b = 5;
// //a = a + b;
// a += b;

// //a = 10 - 5;
// a -= b;

// //a = 10 * 5;
// a *= b;

//a = 10 / 5;
// a /= b;
// console.log(a);

// relacionais (==, !=, <, >, <=, >=) e lógicos (!, &&, ||) e unários (++, --):
// (VARIÁVEIS) + (OPERADORES) + (ESTRUTURAS)
// Estruturas de decisão if/else, ternárias e switch:
// Estruturas de repetição while, do/while, for, for/in e for/of,  arrays.

// const nomes = ["Paulo", "Ana", "Gustavo"];

// for(var i =0; i< nomes.length; i++){
//     var nome = nomes[i];
//     console.log(nome);
// }

// for(i in nomes) {
//     console.log(nomes[i]);
// }

// for(nome of nomes){
//     console.log(nome);
// }

// Funções convencionais e seta.

// function somar(a, b){
//     return a + b;
// }
// console.log(somar(10, 5));

//console.log(((a,b)=>a+b)(10,5));


// function somar(a, b){
//     return a + b;
// }

// const somar = ((a, b) => a + b);

// var result = somar(10,5)
// console.log(result);